<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категорий';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="categories-index">

                <?php Pjax::begin(); ?>

                <p>
                    <?= Html::a('Создать категорию', ['create'], ['class' => 'btn btn-success']) ?>
                </p>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

//                'id',
//                'parent_id',
//                    [
//                        'attribute' => 'parent_id',
//                        'value' => function($data) {
//                            return $data->parent->name;
//                        },
//                    ],
                        'name',
                        'keywords',
                        'description',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
</div>
