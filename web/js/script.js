$(document).on('click', '.add-to-cart', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var nameProduct = $(this).parent().parent().parent().parent().find('.js-name-detail').html();
    swal(nameProduct, "добавлен в корзину !", "success");
    var id = $(this).data('id');
    var qty = $('#item-number').val();
    console.log(qty);
    $.ajax({
        url: '/cart/add',
        data: {id: id, qty: qty},
        type: 'GET',
        success: function (res) {
            $('.icon-header-item').attr("data-notify", res);
        },
        error: function () {
            alert('Error');
        }
    });
});

$(document).on('click', '.shopping-cart', function () {
    $.ajax({
        url: '/cart/items',
        success: function (res) {
            $('#cart-wrapper').html(res);
        },
        error: function () {
            alert('Error');
        }
    });
});

function clearCart() {
    $.ajax({
        url: '/cart/clear',
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка!');
            $('#cart-wrapper').html(res);
            $('.icon-header-item').attr("data-notify", 0);
        },
        error: function () {
            alert('Error');
        }
    });
}