<?php
/**
 * Created by PhpStorm.
 * User: Zhandos
 * Date: 09.02.2019
 * Time: 17:44
 */

namespace app\controllers;


use app\forms\OrderForm;
use app\modules\admin\models\OrderItems;
use yii\helpers\VarDumper;
use yii\web\Controller;
use app\models\Products;
use app\models\Cart;
use Yii;

class CartController extends Controller
{

    public function actionAdd()
    {
        $id = Yii::$app->request->get('id');
        $qty = Yii::$app->request->get('qty');
        $product = Products::findOne($id);
        if (empty($product)) return false;
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->addToCart($product, $qty);
        return $session['cart.qty'];
    }

    public function actionIndex()
    {
        return $this->render('index', compact('session'));
    }

    public function actionItems() {
        if ($_SESSION['cart'])
            $session = $_SESSION['cart'];
            $qty = $_SESSION['cart.qty'];
            $sum = $_SESSION['cart.sum'];
        return $this->renderAjax('items', compact(['session', 'qty', 'sum']));
    }

    public function actionClear() {
        $session = Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.qty');
        $session->remove('cart.sum');
        return $this->renderAjax('items');
    }

    public function actionDetail() {
        $id = Yii::$app->request->get('id');
        $model = Products::findOne($id);
        return $this->renderAjax('detail', compact(['model']));
    }

    public function actionCheckout() {
        if ($_SESSION['cart'])
            $session = $_SESSION['cart'];
        $model = new OrderForm();
        $items = '';
        if ($model->load(Yii::$app->request->get()) && $model->save()) {
            foreach ($session as $item) {
                $items .= '<b>' . $item['name'] . '</b> x ' . $item['qty'] . ' | ' . $item['price'] . ' тг. за ед.<br>';
                $orderItems = new OrderItems();
                $orderItems->name = $item['name'];
                $orderItems->qty = $item['qty'];
                $orderItems->price = $item['price'];
                $orderItems->save();
            }
            Yii::$app->mailer->compose()
                ->setFrom('arigato.site@bk.ru')
                ->setTo('zhandos38@gmail.com') // кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
                ->setSubject('Заказ') // тема письма
                ->setTextBody("Заказ") // текст письма без HTML
                ->setHtmlBody("
                            <h1>Заказ</h1><br>
                            <b>Имя: </b>{$model->name}<br>
                            <b>Телефон: </b>{$model->phone}<br>
                            <b>Адрес: </b>{$model->address}<br>
                            <b>Комментарий: </b>{$model->comments}<br>
                            <hr>
                            {$items}
                            <hr>
                            <b>На сумму: </b>{$_SESSION['cart.sum']} тг.
                                ")
                ->send();
            $session = Yii::$app->session;
            $session->open();
            $session->remove('cart');
            $session->remove('cart.qty');
            $session->remove('cart.sum');
            return $this->redirect(['/']);
        }
        return $this->render('checkout', ['session' => $session, 'model' => $model]);
    }

}