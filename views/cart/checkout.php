<?php
use yii\helpers\Html;
?>
<!-- breadcrumb -->
<div class="container checkout">
    <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
        <a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
            Главная
            <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
        </a>

        <span class="stext-109 cl4">
            Оформление заказа
        </span>
    </div>
</div>


<!-- Shoping Cart -->
<form class="bg0 p-t-75 p-b-85">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
                <div class="m-l-25 m-r--38 m-lr-0-xl">
                    <div class="wrap-table-shopping-cart">
                        <table class="table-shopping-cart">
                            <tr class="table_head">
                                <th class="column-1">Товар</th>
                                <th class="column-2"></th>
                                <th class="column-3">Цена</th>
                                <th class="column-4">Кол-во</th>
                                <th class="column-5">Общее</th>
                            </tr>

                        <?php $totalSum = 0; foreach ($session as $item): ?>
                            <tr class="table_row">
                                <td class="column-1">
                                    <div class="how-itemcart1">
                                        <img src="<?= $item['img'] ?>" alt="IMG">
                                    </div>
                                </td>
                                <td class="column-2"><?= $item['name'] ?></td>
                                <td class="column-3"><?= $item['price'] ?></td>
                                <td class="column-4"><?= $item['qty'] ?></td>
                                <td class="column-5"><?= $item['price']*$item['qty'] ?></td>
                            </tr>
                        <?php $totalSum += $item['price']*$item['qty']; endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>

            <div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
                <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                    <h4 class="mtext-109 cl2 p-b-30">
                        Ваш заказ
                    </h4>

                    <div class="flex-w flex-t bor12 p-b-13">
                        <div class="size-208">
								<span class="stext-110 cl2">
									Сумма:
								</span>
                        </div>

                        <div class="size-209">
								<span class="mtext-110 cl2">
									<?= $totalSum ?> тг.
								</span>
                        </div>
                    </div>

                    <div class="flex-w flex-t bor12 p-t-15 p-b-30">
                        <div class="size-208 w-full-ssm">
								<span class="stext-110 cl2">
									Доставка:
								</span>
                        </div>

                        <div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
                            <p class="stext-111 cl6 p-t-2">
                                There are no shipping methods available. Please double check your address, or contact us if you need any help.
                            </p>

                            <div class="p-t-15">
									<span class="stext-112 cl8">
										Введите ваши данные
									</span>

                                <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'post']) ?>

                                    <?= $form->field($model, 'name')->textInput(['class' => 'bor8 stext-111 cl2 plh3 size-116 p-l-30 p-r-30', 'placeholder' => 'Введите ваше имя'])->label(false) ?>
                                    <?= $form->field($model, 'phone')->textInput(['class' => 'bor8 stext-111 cl2 plh3 size-116 p-l-30 p-r-30', 'placeholder' => 'Введите ваш телефон'])->label(false) ?>
                                    <?= $form->field($model, 'address')->textInput(['class' => 'bor8 stext-111 cl2 plh3 size-116 p-l-30 p-r-30', 'placeholder' => 'Введите ваш адрес'])->label(false) ?>
                                    <div class="bor8 m-b-30">
                                        <?= $form->field($model, 'comments')->textarea(['class' => 'stext-111 cl2 plh3 size-120 p-lr-28 p-tb-25', 'placeholder' => 'Ваши пожелания'])->label(false) ?>
                                    </div>
                                    <?= Html::submitButton('Оформить заказ', ['class' => 'flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 p-lr-15 trans-04 pointer']) ?>

                                <?php $form = \yii\widgets\ActiveForm::end() ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>