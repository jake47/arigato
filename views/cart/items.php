<?php if ($session): ?>
    <ul id="cart" class="header-cart-wrapitem w-full">
    <?php foreach ($session as $id => $item): ?>
            <li class="header-cart-item flex-w flex-t m-b-12">
                <div class="header-cart-item-img">
                    <img src="<?= $item['img'] ?>" alt="IMG">
                </div>

                <div class="header-cart-item-txt p-t-8">
                    <a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
                        <?= $item['name'] ?>
                    </a>

                    <span class="header-cart-item-info">
                        <?= $item['qty'] ?> x <?= $item['price'] ?>
                    </span>
                </div>
            </li>
    <?php endforeach; ?>
    </ul>

    <div class="w-full">
        <div class="header-cart-total w-full p-tb-40">
            Сумма: <?= $sum ?> тг.
        </div>

        <div class="header-cart-buttons flex-w w-full">
            <a id="clear-cart" href="javascript:void(0)" onclick="clearCart()" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
                Очистить корзину
            </a>

            <a href="<?= \yii\helpers\Url::to(['cart/checkout']) ?>" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
                Оформить заказ
            </a>
        </div>
    </div>
<?php else: ?>
    <h4>Корзина пуста</h4>
<?php endif; ?>
