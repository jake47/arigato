<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'Arigato';
$imgPath = Yii::getAlias('@web') . '/images/';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!-- Header -->
<header class="header-v2">
<!-- Header desktop -->
<div class="container-menu-desktop">
    <!-- Topbar -->
    <div class="top-bar">
        <div class="content-topbar flex-sb-m h-full container">
            <div class="left-top-bar">
                г. Шымкент | Бесплатная доставка от 3000 тенге.
            </div>

            <div class="right-top-bar flex-w h-full">
                <a href="#" class="js-show-modal2 flex-c-m trans-04 p-lr-25">
                    Заказать обратный звонок
                </a>
                <a href="#" class="flex-c-m trans-04 p-lr-25">
                    Связаться с боссом
                </a>
                <a href="#" class="flex-c-m trans-04 p-lr-25">
                    +7 (707) 137 20 20
                </a>

                <a href="#" class="flex-c-m trans-04 p-lr-25">
                    +7 (771) 561 20 20
                </a>

                <a href="#" class="flex-c-m trans-04 p-lr-25">
                    +7 (775) 139 20 20
                </a>
            </div>
        </div>
    </div>

    <div class="wrap-menu-desktop how-shadow1" style="top: 40px;">
        <nav class="limiter-menu-desktop container">

            <!-- Logo desktop -->
            <a href="<?= Url::to(['/']) ?>" class="logo">
                <img src="<?= $imgPath . 'logo.png' ?>" alt="IMG-LOGO">
            </a>

            <!-- Menu desktop -->
            <div class="menu-desktop">
                <ul class="main-menu">
                    <!-- <li class="active-menu"> -->
                    <!-- <a href="index.html">Домой</a> -->
                    <!-- <ul class="sub-menu"> -->
                    <!-- <li><a href="index.html">Homepage 1</a></li> -->
                    <!-- <li><a href="home-02.html">Homepage 2</a></li> -->
                    <!-- <li><a href="home-03.html">Homepage 3</a></li> -->
                    <!-- </ul> -->
                    <!-- </li> -->

                    <li>
                        <a href="<?= Url::to(['/']) ?>">Главная</a>
                    </li>

                    <!-- <li class="label1" data-label1="hot"> -->
                    <!-- <a href="shoping-cart.html">Корзина</a> -->
                    <!-- </li> -->

                    <li>
                        <a href="http://arigato.loc/#menu">Меню</a>
                    </li>

                    <li>
                        <a href="<?= Url::to(['site/about']) ?>">О ресторане</a>
                    </li>

                    <li>
                        <a href="<?= Url::to(['site/contact']) ?>">Контакты</a>
                    </li>
                </ul>
            </div>

            <!-- Icon header -->
            <div class="wrap-icon-header flex-w flex-r-m">

                <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" data-notify="<?= $_SESSION['cart.qty'] ? $_SESSION['cart.qty'] : 0 ?>">
                    <i class="shopping-cart zmdi zmdi-shopping-cart"></i>
                </div>

            </div>
        </nav>
    </div>
</div>

<!-- Header Mobile -->
<div class="wrap-header-mobile">
    <!-- Logo moblie -->
    <div class="logo-mobile">
        <a href="index.html"><img src="images/icons/logo-01.png" alt="IMG-LOGO"></a>
    </div>

    <!-- Icon header -->
    <div class="wrap-icon-header flex-w flex-r-m m-r-15">

        <div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti js-show-cart" data-notify="2">
            <i class="shopping-cart zmdi zmdi-shopping-cart"></i>
        </div>

    </div>

    <!-- Button show menu -->
    <div class="btn-show-menu-mobile hamburger hamburger--squeeze">

        <span class="hamburger-box">
            <span class="hamburger-inner"></span>
        </span>

    </div>
</div>


<!-- Menu Mobile -->
<div class="menu-mobile">
    <ul class="topbar-mobile">

        <li>
            <div class="right-top-bar flex-w h-full">
                <a href="#" class="flex-c-m p-lr-10 trans-04">
                    +7 (707) 137 20 20
                </a>

                <a href="#" class="flex-c-m p-lr-10 trans-04">
                    +7 (771) 561 20 20
                </a>

                <a href="#" class="flex-c-m p-lr-10 trans-04">
                    +7 (775) 139 20 20
                </a>
            </div>
        </li>

    </ul>

    <ul class="main-menu-m">
        <li>
            <a href="<?= Url::to(['/']) ?>">Главная</a>
        </li>

        <li>
            <a href="http://arigato.loc/#menu">Меню</a>
        </li>

        <li>
            <a href="<?= Url::to(['site/about']) ?>" class="label1 rs1" data-label1="hot">О ресторане</a>
        </li>
        <li>
            <a href="<?= Url::to(['site/contact']) ?>">Контакты</a>
        </li>
    </ul>
</div>
</header>

<!-- Cart -->
<div class="wrap-header-cart js-panel-cart">
    <div class="s-full js-hide-cart"></div>

    <div class="header-cart flex-col-l p-l-65 p-r-25">
        <div class="header-cart-title flex-w flex-sb-m p-b-8">
				<span class="mtext-103 cl2">
					Корзина
				</span>

            <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
                <i class="zmdi zmdi-close"></i>
            </div>
        </div>

        <div id="cart-wrapper" class="header-cart-content flex-w js-pscroll">

        </div>
    </div>
</div>

        <?= Alert::widget() ?>
        <?= $content ?>

<!--Addvantages-->

<!-- Footer -->
<footer class="bg3 p-t-75 p-b-32">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Наши контакты
                </h4>

                <ul>
                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            +7 (707) 137 20 20
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            +7 (771) 561 20 20
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            +7 (775) 139 20 20
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            info@arigato.kz
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Инфо
                </h4>

                <ul>
                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            Track Order
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            Returns
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            Shipping
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            FAQs
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Оставатся на связи
                </h4>

                <p class="stext-107 cl7 size-201">
                    Any questions? Дай нам знать
                </p>

                <div class="p-t-27">
                    <a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-facebook"></i>
                    </a>

                    <a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-instagram"></i>
                    </a>

                    <a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-pinterest-p"></i>
                    </a>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
<!--                <h4 class="stext-301 cl0 p-b-30">-->
<!--                    Новостная лента-->
<!--                </h4>-->

<!--                <form>-->
<!--                    <div class="wrap-input1 w-full p-b-4">-->
<!--                        <input class="input1 bg-none plh1 stext-107 cl7" type="text" name="email" placeholder="email@example.com">-->
<!--                        <div class="focus-input1 trans-04"></div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="p-t-18">-->
<!--                        <button class="flex-c-m stext-101 cl0 size-103 bg1 bor1 hov-btn2 p-lr-15 trans-04">-->
<!--                            Подписатся-->
<!--                        </button>-->
<!--                    </div>-->
<!--                </form>-->
            </div>
        </div>

        <div class="p-t-40">
            <p class="stext-107 cl6 txt-center">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

            </p>
        </div>
    </div>
</footer>


<!-- Back to top -->
<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
