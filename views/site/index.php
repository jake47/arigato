<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Arigato';
$imgPath = Yii::getAlias('@web') . '/images/';

?>
<!-- Slider -->
<section class="section-slide">
    <div class="wrap-slick1">
        <div class="slick1">
            <div class="item-slick1" style="background-image: url(images/slider/slide1.jpg);">
                <div class="container h-full">

                    <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
                        <div class="layer-slick1 animated visible-false" data-appear="fadeInDown" data-delay="0">
                            <span class="ltext-101 cl2 respon2">
                                Доставка по г. Шымкент
                            </span>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn" data-delay="800">
                            <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
                                Pizza & Sushi
                            </h2>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1600">
                            <a href="product.html" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                                Попробовать сейчас
                            </a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="item-slick1" style="background-image: url(images/slider/slide2.jpg);">
                <div class="container h-full">

                    <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
                        <div class="layer-slick1 animated visible-false" data-appear="rollIn" data-delay="0">
								<span class="ltext-101 cl2 respon2">
									Men New-Season
								</span>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn" data-delay="800">
                            <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
                                Jackets & Coats
                            </h2>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="slideInUp" data-delay="1600">
                            <a href="product.html" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                                Попробовать сейчас
                            </a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="item-slick1" style="background-image: url(images/slider/slide3.jpg);">
                <div class="container h-full">

                    <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
                        <div class="layer-slick1 animated visible-false" data-appear="rollIn" data-delay="0">
								<span class="ltext-101 cl2 respon2">
									Men New-Season
								</span>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn" data-delay="800">
                            <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
                                Pizza & Sushi
                            </h2>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="slideInUp" data-delay="1600">
                            <a href="product.html" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                                Попробовать сейчас
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Product -->
<section class="bg0 p-t-23 p-b-80">
    <div class="container">
        <div class="p-b-10">
            <h3 class="ltext-103 cl5 text-center" id="menu">
                Наше меню
            </h3>
        </div>

        <div class="flex-w flex-sb-m p-b-52 d-flex justify-content-center">
            <div class="flex-w flex-l-m filter-tope-group m-tb-10">
                <?php $isActive = false; ?>
                <?php foreach ($categories as $category): ?>

                <button <?= !$isActive ? 'id="first-section-btn"' : '' ?> class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".<?= $category->id ?>" <?= !$isActive ? 'active' : '' ?>>
                    <?= $category->name ?>
                </button>

                <?php $isActive = true; ?>
                <?php endforeach; ?>
            </div>


            <!-- Search product -->
            <div class="dis-none panel-search w-full p-t-10 p-b-15">
                <div class="bor8 dis-flex p-l-15">
                    <button class="size-113 flex-c-m fs-16 cl2 hov-cl1 trans-04">
                        <i class="zmdi zmdi-search"></i>
                    </button>

                    <input class="mtext-107 cl2 size-114 plh2 p-r-15" type="text" name="search-product" placeholder="Search">
                </div>
            </div>
        </div>

        <div class="row isotope-grid">
            <?php foreach ($categories as $category): ?>
                <?php foreach ($category->products as $product): ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item <?= $category->id ?>">
                            <!-- Block2 -->
                            <div class="block2">
                                <div class="block2-pic hov-img0">
                                    <img src="<?php $productImg = $product->getImage(); echo $productImg->getUrl('300x300')?>" alt="IMG-PRODUCT">

                                    <a href="<?= \yii\helpers\Url::to(['cart/add', 'id' => $product->id]) ?>" data-id="<?= $product->id ?>" class="js-show-modal1 block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04">
                                        Подробнее
                                    </a>
                                </div>

                                <div class="block2-txt flex-w flex-t p-t-14">
                                    <div class="block2-txt-child1 flex-col-l ">
                                        <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                            <?= $product->name ?>
                                        </a>

                                        <span class="stext-105 cl3">
                                            <?= $product->price ?> тг.
                                        </span>
                                    </div>

                                    <div class="block2-txt-child2 flex-r p-t-3">
                                        <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                            <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                            <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php endforeach; ?>
            <?php endforeach; ?>

        </div>

    </div>
</section>

<!-- Modal1 -->
<div class="wrap-modal1 js-modal1 p-t-60 p-b-20">
    <div class="overlay-modal1 js-hide-modal1"></div>

    <div class="container">
        <div class="item-detail bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">

        </div>
    </div>
</div>

<!-- Modal1 -->
<div class="wrap-modal2 js-modal2 p-t-60 p-b-20">
    <div class="overlay-modal2 js-hide-modal2"></div>

    <div class="container feedback-wrapper">
        <div class="feedback bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">
            <div class="container">
                <?php $form = \yii\widgets\ActiveForm::begin() ?>
                    <h4 class="mtext-105 cl2 txt-center p-b-30">
                        Связаться с менеджером
                    </h4>
                    <?= $form->field($model, 'name')->textInput(['class' => 'bor8 stext-111 cl2 plh3 size-116 p-l-30 p-r-30', 'placeholder' => 'Введите ваше имя'])->label(false) ?>
                    <?= $form->field($model, 'phone')->textInput(['class' => 'bor8 stext-111 cl2 plh3 size-116 p-l-30 p-r-30', 'placeholder' => 'Введите ваш телефон'])->label(false) ?>
                    <div class="bor8 m-b-30">
                        <?= $form->field($model, 'comments')->textarea(['class' => 'stext-111 cl2 plh3 size-120 p-lr-28 p-tb-25', 'placeholder' => 'Чем мы можем помочь?'])->label(false) ?>
                    </div>
                    <?= Html::submitButton('Отправить', ['class' => 'flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 p-lr-15 trans-04 pointer']) ?>
                <?php $form = \yii\widgets\ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php

$js = <<<JS

$(document).ready(function() {
    $('#first-section-btn').click();
});

JS;

$this->registerJs($js);


?>
