<?php
namespace app\forms;

use yii\db\ActiveRecord;

class OrderForm extends ActiveRecord
{
    public static function tableName() {
        return '{{orders}}';
    }

    public function rules()
    {
        return [
            [['name', 'phone', 'address'], 'required'],
            [['name', 'phone', 'address', 'comments'], 'string']
        ];
    }
}