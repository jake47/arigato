<?php
namespace app\forms;

class FeedbackForm extends \yii\base\Model
{
    public $name;
    public $phone;
    public $comments;

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['name', 'phone', 'comments'], 'string']
        ];
    }
}