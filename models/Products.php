<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $content
 * @property double $price
 * @property string $keywords
 * @property string $description
 * @property string $img
 * @property int $hit
 * @property int $new
 * @property int $sale
 */
class Products extends \yii\db\ActiveRecord
{
    public $imgFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['category_id', 'hit', 'new', 'sale'], 'integer'],
            [['content'], 'string'],
            [['price'], 'number'],
            [['name', 'keywords', 'description', 'img'], 'string', 'max' => 255],
            [['imgFile'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'name' => 'Названия',
            'content' => 'Контент',
            'price' => 'Цена',
            'keywords' => 'Ключевые слова',
            'description' => 'Описания (Мета)',
            'imgFile' => 'Рисунок',
            'hit' => 'Хит',
            'new' => 'Новый',
            'sale' => 'Скидка',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $path = 'images/products/' . $this->imgFile->baseName . '.' . $this->imgFile->extension;
            $this->imgFile->saveAs($path);
            $this->attachImage($path);
            return true;
        } else {
            return false;
        }
    }

    public function getCategory()
    {
        return $this->hasOne(Categories::class,['id' => 'category_id']);
    }
}
